//
//  Screenshot.swift
//  Break
//
//  Created by Hugo Machado on 4/8/15.
//  Copyright (c) 2015 Beatriz Melo Mousinho Magalhães. All rights reserved.
//

import UIKit

class Screenshot: NSObject {
    
    func screenShotMethod(aux: UIView)->UIImage{
        var temp:UIImage! = UIImage()
        UIGraphicsBeginImageContextWithOptions(UIScreen.mainScreen().bounds.size, false, 0);
        aux.drawViewHierarchyInRect(aux.bounds, afterScreenUpdates: true)
        temp = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        println("Screenshot")
        
        return temp
        
    }
}
