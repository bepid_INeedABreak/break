//
//  TimeBreakVC.swift
//  Break
//
//  Created by Abdallah Assaad Seoud on 3/14/15.
//  Copyright (c) 2015 Beatriz Melo Mousinho Magalhães. All rights reserved.
//

import UIKit

class TimeBreakVC: UIViewController {
    
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var goView: UIView!
    
    var previousLocation = CGPoint()
    
    //Screensize
    var width = UIScreen.mainScreen().bounds.size.width
    var height = UIScreen.mainScreen().bounds.size.height

    
    @IBAction func timeSliderChanged(sender: UISlider) {
        
        var currentValue = Int(sender.value)

        labelTime.text = "\(currentValue) minutes"
        var size:Float = 20+(sender.value)/1.2
        labelTime.font = labelTime.font.fontWithSize(CGFloat(size))
        //labelTime.sizeToFit()
        labelTime.textAlignment = .Center
        //size = -size
        //labelTime.frame.origin.x.advancedBy(CGFloat(size))
       // labelTime.layer.anchorPoint = labelTime.frame.x/2.
        
    }
    
       @IBAction func goButton(sender: AnyObject) {
        
        var peopleView: PeopleBreakVC = self.storyboard?.instantiateViewControllerWithIdentifier("PeopleBreakVC") as! PeopleBreakVC
        
        var currentValue = Int(self.timeSlider.value)
        
        if currentValue <= 10 {
            SystemStatus.sharedInstance.timeString = "10"
            println("menor que 10")
        }
            
        else{
            SystemStatus.sharedInstance.timeString = "20"
            println("maior que 10")
        }
        
        self.showViewController(peopleView, sender: self)
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  ANIMATING BUTTONS
                       
        //time slider
        self.animButton(timeSlider, delayTime: 0.1, duration: 0.7)
        
        //home
        self.animButton(goView, delayTime: 0.05, duration: 0.7)

    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        //CUSTOMIZING NAVIGATION BAR
        
        let swiftColor = UIColor(red: 1, green: 205/255, blue: 93/255, alpha: 1)
        navigationController?.navigationBar.tintColor = swiftColor
        
        navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        
        let backButton = UIBarButtonItem(title: "< back", style: UIBarButtonItemStyle.Plain, target: self, action: "goBack:")
        navigationItem.leftBarButtonItem = backButton
        navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Geeza Pro", size: 20)!], forState: UIControlState.Normal)
    }
    func goBack(sender:UIBarButtonItem)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func animButton(button: UIView, delayTime: Double, duration: Double){
        
        button.frame.origin.x = self.width
        button.alpha = 0.05
        self.view.addSubview(button)
        
        UIView.animateWithDuration(duration, delay:delayTime, options: nil, animations: {
            button.alpha = 1.0
            button.frame.origin.x = self.width*0.34
            
            }, completion: nil)
        
    }

    
}
