//
//  LoginScreenVC.swift
//  Break
//
//  Created by Beatriz Melo Mousinho Magalhães on 3/13/15.
//  Copyright (c) 2015 Beatriz Melo Mousinho Magalhães. All rights reserved.
//

import UIKit

class LoginScreenVC: UIViewController, FBLoginViewDelegate {
    
    @IBOutlet weak var subView1: UIView!
    @IBOutlet var fbLoginView: FBLoginView!
    var soon:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.fbLoginView.delegate = self
//        self.fbLoginView.readPermissions = ["public_profile", "email","user_friends"]
//        
        
        //TODO: Make facebook login work
  //      fbLoginView.hidden = true
//        fbLoginView.alpha = 0.3
//        fbLoginView.userInteractionEnabled = false
//        
//        soon = UILabel()
//        soon.frame = fbLoginView.frame
//        soon.font = UIFont(name: "Futura-Medium", size: 34)
//        soon.text = "coming soon"
//        soon.textColor = UIColor.whiteColor()
//        soon.textAlignment = NSTextAlignment.Center
//        soon.alpha = 0.8
//        self.subView1.addSubview(soon)
//        soon.frame.origin.y = 18
//        soon.frame.origin.x = 15
        
    }
    
    // Facebook Delegate Methods
    func loginViewShowingLoggedInUser(loginView: FBLoginView!) {
        
        performSegueWithIdentifier("toFrom", sender: self)
        
        println("User  Logged In")
        println("This is where you perform a segue")
    }
    
    func loginViewFetchedUserInfo(loginView: FBLoginView!, user: FBGraphUser!) {
        println("User name: \(user.name)")
    }
    
    func loginViewShowingLoggedOutUser(loginView: FBLoginView!) {
        println("User logged Out")
    }
    
    func loginView(loginView: FBLoginView!, handleError: NSError!) {
        println("Error: \(handleError.localizedDescription)")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = true
    }
    
    
    
    
}

