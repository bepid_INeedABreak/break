//
//  AnimationVC.swift
//  Break
//
//  Created by Tamyres Freitas on 4/1/15.
//  Copyright (c) 2015 Beatriz Melo Mousinho Magalhães. All rights reserved.
//

import UIKit

class AnimationVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    let transitionManager = TransitionVC()
    
    var bg: UIImageView! = UIImageView()
    var screenShot:UIImage! = UIImage()
    var editedImage:UIImage! = UIImage()
    var imagePicker:UIImagePickerController! = UIImagePickerController()
    var width = UIScreen.mainScreen().bounds.size.width
    var height = UIScreen.mainScreen().bounds.size.height
    var tapCount:Int!
    var soon: UILabel!

    @IBOutlet weak var stbImg: UIImageView!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet weak var faceButton: UIButton!
    @IBOutlet weak var faceLabel: UILabel!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var anotherButton: UIButton!
    @IBOutlet weak var anotherLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    
    
    @IBAction func share(sender: AnyObject) {
        self.shootPhoto()
    }

    @IBAction func cancelButton(sender: UIStoryboardSegue) {
            self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func anotherAction(sender: AnyObject) {
        
        //TODO: fix restrictedAccess
        //tapCount=tapCount+1
        //println("numero \(tapCount)")
        
        if tapCount > 6 {
            
            anotherButton.hidden = true
            anotherLabel.hidden = true
            cancelButton.hidden = true
            backView.hidden = true
            
            
            var alert = UIAlertController(title: "OPS!", message: String(format: "Come after %.0f minutes", 120-(NSDate().timeIntervalSinceDate(NSUserDefaults.standardUserDefaults().objectForKey("date") as! NSDate)/60.0)), preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            self.presentViewController(alert, animated: true, completion: nil)
            
            
        }
        else{
            
            var breakView: BreakVC = self.storyboard?.instantiateViewControllerWithIdentifier("BVC") as! BreakVC
            breakView.tapCount = self.tapCount
            self.presentViewController(breakView, animated: true, completion: nil)
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        if tapCount >= 6 {
            
            anotherButton.hidden = true
            anotherLabel.hidden = true
            cancelButton.hidden = true
            backView.hidden = true
            
        }
        
        bg.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        bg.alpha = 0.5
        stbImg.image = bg.image
        stbImg.frame.origin = CGPointMake(0, 0)
        
//        //TODO: Depends on facebook login
//        faceButton.alpha = 0.4
//        faceButton.userInteractionEnabled = false
//        faceLabel.alpha = 0.4
//        
//        soon = UILabel()
//        soon.frame = CGRectMake(width/2 - 100, faceButton.frame.origin.y, 200, 60)
//        soon.textColor = UIColor.whiteColor()
//        soon.textAlignment = NSTextAlignment.Center
//        
//        soon.font = UIFont(name: "Futura-Medium", size: 34)
//        soon.text = "coming soon"
//        soon.alpha = 0.8
//        self.view.addSubview(soon)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.transitioningDelegate = self.transitionManager
        
        if tapCount == nil{
            
            tapCount = 0
            
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        self.screenShot = Screenshot().screenShotMethod(self.view)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func setView1(obj:UIImage){
        
        bg = UIImageView()
        bg.image = obj
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        var newView:UIView! = UIView()
        newView.frame.size = self.view.frame.size
        
        var img1:UIImageView! = UIImageView()
        img1.image = bg.image
        img1.frame.size = self.view.frame.size
        
        var img2:UIImageView! = UIImageView()
        img2.image = (info[UIImagePickerControllerOriginalImage] as! UIImage)
        img2.frame = CGRectMake(0, self.view.frame.size.height * (1/3.0), self.view.frame.size.width, self.view.frame.size.height)
        newView.addSubview(img1)
        newView.addSubview(img2)
        
        self.editedImage = Screenshot().screenShotMethod(newView)
        
        let photo : FBSDKSharePhoto = FBSDKSharePhoto()
        photo.image = self.editedImage
        photo.userGenerated = true
        let content : FBSDKSharePhotoContent = FBSDKSharePhotoContent()
        content.photos = [photo]
        
        FBSDKShareDialog.showFromViewController(self, withContent: content, delegate: nil)
        println("Posted photo")
        
    }
    
    func shootPhoto() {
        if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
            
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.cameraCaptureMode = .Photo
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertVC = UIAlertController(title: "No Camera", message: "Sorry, this device has no camera", preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style:.Default, handler: nil)
            alertVC.addAction(okAction)
            presentViewController(alertVC, animated: true, completion: nil)
        }
        
    }
    
    
}




