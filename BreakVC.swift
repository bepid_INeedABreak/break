//
//  BreakVC.swift
//  Break
//
//  Created by Stefano Politi on 16/03/15.
//  Copyright (c) 2015 Beatriz Melo Mousinho Magalhães. All rights reserved.
//

import UIKit
import WebKit


class BreakVC: UIViewController {
    
    var webView: WKWebView?
    var locked:Bool = false
    var brk: Break?
    var dao:DAO = DAO()
    var tapCount:Int!
    var width = UIScreen.mainScreen().bounds.size.width
    var height = UIScreen.mainScreen().bounds.size.height
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //TODO: fix access restriction
        //self.verifyAccess();
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if tapCount == nil{
            
            tapCount = 0
            
        }
        
        self.brk = dao.loadBreak(SystemStatus.sharedInstance.placeString, time: SystemStatus.sharedInstance.timeString, people: SystemStatus.sharedInstance.optionString)
        
        label.text? = self.brk!.dsc
        titleLabel.text = self.brk!.title
        
        
        if self.brk!.type == BreakType.image {
            var breakImageView:UIImageView = UIImageView(image: self.brk!.img)
            breakImageView.frame = CGRect(x:0, y:height/3.00, width:width, height: height/2)
            breakImageView.contentMode = .ScaleAspectFit
            view.addSubview(breakImageView)
            println("\(breakImageView.frame)")
            
            
        } else if self.brk!.type == BreakType.video {
            
            self.webView = self.brk?.vid
            self.webView?.frame = CGRect(x:0, y:height/2.8, width:width, height: height/2.5)
            self.view.addSubview(self.webView!)
            
        }
        
    }
    
    @IBAction func animationButton(sender: AnyObject) {
        
        var animationView: AnimationVC = self.storyboard?.instantiateViewControllerWithIdentifier("AnimationVC") as! AnimationVC
        animationView.tapCount = self.tapCount
        animationView.setView1( Screenshot().screenShotMethod(self.view))
        
        self.showViewController(animationView, sender: self)
        
    }
    
    func verifyAccess(){
        
        if(NSUserDefaults.standardUserDefaults().objectForKey("date")==nil && (tapCount != nil && tapCount >= 6)){
            
            NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey:"date")
            println("Saved for the first time")
        }
        
        if(NSUserDefaults.standardUserDefaults().objectForKey("date") != nil &&  (NSDate().timeIntervalSinceDate(NSUserDefaults.standardUserDefaults().objectForKey("date") as! NSDate) >= 3600*(1/2))){
            
            NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey:"date")
            locked = false
            tapCount = 0
            println("After 2 hours")
            
        }
        
        if(NSUserDefaults.standardUserDefaults().objectForKey("date") != nil &&  (NSDate().timeIntervalSinceDate(NSUserDefaults.standardUserDefaults().objectForKey("date") as! NSDate) < 3600*(1/2))){
            
            locked = true
            println("You still cannot use it")
            
        }
        
        print(NSUserDefaults.standardUserDefaults().objectForKey("date"))
        
        
        if(locked == true){
            println("dismissed")
            
            self.view.hidden = true
            
            var alert = UIAlertController(title: "OPS!", message: String(format: "Come after %.0f minutes", 30-(NSDate().timeIntervalSinceDate(NSUserDefaults.standardUserDefaults().objectForKey("date") as! NSDate)/60.0)), preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in self.dismissViewControllerAnimated(false, completion: nil)}))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
}






