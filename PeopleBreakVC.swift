//
//  PeopleBreakVCVC.swift
//  Break
//
//  Created by Beatriz Melo Mousinho Magalhães on 3/15/15.
//  Copyright (c) 2015 Beatriz Melo Mousinho Magalhães. All rights reserved.
//

import UIKit

class PeopleBreakVC: UIViewController {
    
    @IBOutlet weak var yayView: UIView!
    @IBOutlet weak var nayView: UIView!
    
    //Screensize
    var width = UIScreen.mainScreen().bounds.size.width
    var height = UIScreen.mainScreen().bounds.size.height

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NAVIGATION BAR CUSTOMIZING
        let swiftColor = UIColor(red: 163/255, green: 240/255, blue: 153/255, alpha: 1)
        navigationController?.navigationBar.tintColor = swiftColor


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func yesAction(sender: AnyObject) {
        
        var breakView: BreakVC = self.storyboard?.instantiateViewControllerWithIdentifier("BVC") as! BreakVC
        
        SystemStatus.sharedInstance.optionString = "p"
        
        self.presentViewController(breakView, animated: true, completion: nil)
        
        
    }

    @IBAction func noAction(sender: AnyObject) {
        
        var breakView: BreakVC = self.storyboard?.instantiateViewControllerWithIdentifier("BVC") as! BreakVC
        
        SystemStatus.sharedInstance.optionString = "np"

        self.presentViewController(breakView, animated: true, completion: nil)

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        //CUSTOMIZING NAVIGATION BAR
        
        let swiftColor = UIColor(red: 163/255, green: 240/255, blue: 153/255, alpha: 1)
        navigationController?.navigationBar.tintColor = swiftColor
        
        let backButton = UIBarButtonItem(title: "< back", style: UIBarButtonItemStyle.Plain, target: self, action: "goBack:")
        navigationItem.leftBarButtonItem = backButton
        navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Geeza Pro", size: 20)!], forState: UIControlState.Normal)
        
        //  ANIMATING BUTTONS
        
        
        //yay
        self.animButton(yayView, delayTime: 0.1, duration: 0.7)
        
        //nay
        self.animButton(nayView, delayTime: 0.05, duration: 0.7)

    }
    
    func goBack(sender:UIBarButtonItem)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func animButton(button: UIView, delayTime: Double, duration: Double){
        
        button.frame.origin.x = self.width
        button.alpha = 0.05
        self.view.addSubview(button)
        
        UIView.animateWithDuration(duration, delay:delayTime, options: nil, animations: {
            button.alpha = 1.0
            button.frame.origin.x = self.width*0.34
            
            }, completion: nil)
        
    }


}
